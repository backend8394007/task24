const express = require('express');
const path = require('path');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/home', (req, res) =>{
    res.render('index', {title: 'Home page'});
});

app.get('/about', (req, res) =>{
    res.render('index', {title: 'About page'});
});

const port = 3000;
app.listen(port, () => console.log(`The server is listening on port: ${port}`));

/** Хотел бы узнать, какие мидлвары дейсвтительно используются в проектах? */
